package com.restservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PhoneBookController {
	@GetMapping("/")
	public PhoneBook greeting(@RequestParam(value = "filename", defaultValue = "") String fileName) {
		return new PhoneBook(fileName);
	}
}
