package com.restservice;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class PhoneBook {

	private HashMap<String, String> contactMap;
	private String upload_dir_path = "../../upload_service/complete/upload-dir/";

	public PhoneBook(String fileName) {
		contactMap = new HashMap<>();
		contactMap.put("Adam", "555-7070");
		contactMap.put("Benjamin", "555-7537");
		contactMap.put("Charlie", "555-7179");
		
		if(!fileName.equals("")) {
			parseXMLFile(fileName);
		}
	}

	private void parseXMLFile(String fileName) {
		try {
	         File inputFile = new File(upload_dir_path + fileName);
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         NodeList nList = doc.getElementsByTagName("contact");
	         
	         for (int i = 0; i < nList.getLength(); i++) {
	            Node node = nList.item(i);
	            
	            if (node.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) node;
	               contactMap.put(eElement
	                  .getElementsByTagName("name")
	                  .item(0)
	                  .getTextContent(), 
	                  eElement
	                  .getElementsByTagName("number")
	                  .item(0)
	                  .getTextContent());
	            }
	         }
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	}

	public HashMap<String, String> getContactMap() {
		return contactMap;
	}

}
