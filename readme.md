About this project:
1. phonebook_service is used to return a phonebook in JSON format while upload_service is used to upload an XML-file containing a phonebook. An example of the expected format for XML phonebooks can be found under the 'contacts folder'.

2. For demonstration purposes, phonebook_service is hosted on localhost:8080 while upload_service is hosted on localhost:8081. If an XML file is uploaded, the filename can be specified as localhost=8080?filename=contacts.xml, for instance.

3. This project was based on the Spring RESTful web service.

